#include "List.h"

// constructor
List::List() { size = 0; }


// to add an item to the back of the list (append)
bool List::add(ItemType item)//This is only to store the code of the interchanges
{
	bool success = size < MAX_SIZE_LIST;
	if (success)
	{
		items[size] = item;    // add to the end of the list
		size++;                // increase the size by 1
	}
	return success;
}


// to retrieve an item in the array, given its index
ItemType List::getArrayItem(int index)
{
	bool success = (index >= 0) && (index < size);
	if (success)
	{
		return items[index];
	}
	else
	{
		return "";
	}
}
