#include "Map.h"

// constructor of map
Map::Map()
{
	size = 0;
	for (int i = 0; i < MAX_SIZE; i++)
	{
		lines[i] = NULL;	// creates an array of 30 items (stations) which are defaulted to NULL
	}
}


// to retrieve all station data from Stations.csv
bool Map::addStationData(string code, string name)
{
	Node* newNode = new Node;
	newNode->code = code;
	newNode->name = name;
	newNode->previous = NULL;
	newNode->next = NULL;
	newNode->distance = 0;
	int index = getLineIndex(code);
	Node* current = lines[index];

	if (current == NULL)
	{
		lines[index] = newNode;
	}
	else	// if there is at least one station in the line
	{
		while (current->next != NULL)
		{
			current = current->next;
		}
		current->next = newNode;
		newNode->previous = current;
	}
	size++;
	return true;
}


// to retrieve and store all station distances from Routes.csv
void Map::loadDistance(string code, string distance)
{
	Node* current = returnNode(code);
	current->distance = stoi(distance);
}


// to retrieve and store all stations with 2 interchanges from Interchanges.csv
void Map::loadInterchange(string s1, string s2)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		Node* current = lines[i];

		while (current != NULL)
		{
			if (current->code == s1)
			{
				current->interchanges[0] = s2;
			}
			current = current->next;
		}
	}
}


// to retrieve and store all stations with 3 interchanges from Interchanges.csv
void Map::loadInterchange(string s1, string s2, string s3)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		Node* current = lines[i];
		while (current != NULL)
		{
			if (current->code == s1)
			{
				current->interchanges[0] = s2;
				current->interchanges[1] = s3;
			}
			current = current->next;
		}
	}
}


// to retrieve the index of a line (EW, CC, NS) in the lines array, given a station code
int Map::getLineIndex(string code)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		if (lines[i] == NULL)
		{
			return i;
		}
		else
		{
			string input_linecode = code.substr(0, 2);	// retrieves line code (EW) of the code input
			string array_linecode = (lines[i]->code).substr(0, 2); // retrieves line code (EW) of the first index in array

			if (input_linecode == array_linecode)
			{
				return i;
			}
		}
	}
}


// to determine if a station exists, given the station code
bool Map::stationValidityCode(string code)
{
	Node* current = lines[getLineIndex(code)];	// move current ptr to the first station of NS line
	while (current != NULL)
	{
		if (current->code == code)
		{
			return true;
		}
		current = current->next;	//traverse between the stations of the same line
	}
	return false;
}


// to determine if a station exists throughout all lines, given the station name
bool Map::stationValidityName(string name)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		Node* current = lines[i];
		while (current != NULL)
		{
			if (current->name == name)
			{
				return true;
			}
			current = current->next;	//traverse between the stations of the same line
		}
	}
	return false;
}


// to determine if a line exists
bool Map::lineValidity(string line)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		if (lines[i] == NULL)
		{
			return false;
		}
		else
		{
			string array_line = (lines[i]->code).substr(0, 2); // retrieves line code (EW) of the first index in array

			if (line == array_line)
			{
				return true;
			}
			return false;
		}
	}
}


// to determine if a station exists in a line, given the station name and code
bool Map::stationExistInLine(string name, string code)	// e.g. (Woodlands, NS)
{
	Node* current = lines[getLineIndex(code)];
	while (current != NULL)
	{
		if (current->name == name)
		{
			return true;
			break;
		}
		current = current->next;//traverse between the stations of the same line
	}
	return false;
}


// Option 1 : to display all stations of a line
void Map::displayStationsOfLine(string code)
{
	Node* current = lines[getLineIndex(code)];
	cout << endl;
	if (current == NULL)
	{
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
		cout << "Invalid line code entered!" << endl;
		cout << "~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~" << endl;
	}
	else
	{
		cout << "------------------------------------" << endl;
		cout << code << " Line Code\tStation Name" << endl;
		cout << "------------------------------------" << endl;

		Node* current = lines[getLineIndex(code)];

		while (current != NULL)
		{
			cout << current->code << "\t\t" << current->name << endl;
			current = current->next;
		}
		cout << endl;
	}
}


// Option 2 : to retrieve station information, given the station name
void Map::getStationInfo(string name)
{
	for (int i = 0; i < MAX_SIZE; i++)
	{
		Node* current = lines[i];

		while (current != NULL)
		{
			if (current->name == name)
			{
				cout << endl;
				cout << "Station code : " << current->code << endl;
				if (current->previous != NULL)
				{
					cout << "Distance to " << current->previous->name << " (" << current->previous->code << ") : " << current->previous->distance << " metres" << endl;
				}
				if (current->next != NULL)
				{
					cout << "Distance to " << current->next->name << " (" << current->next->code << ") : " << current->distance << " metres" << endl;
				}
			}
			current = current->next;
		}
	}
	cout << endl;
}


// Option 3 : to add a station to a line
bool Map::addStation(string code, string name, char beforeafter, string reference, int distance)
{
	int index = getLineIndex(code);
	Node* newNode = new Node;
	newNode->code = code;
	newNode->name = name;
	newNode->previous = NULL;
	newNode->next = NULL;

	bool existingStationName = stationValidityName(name);	// true = add existing station codes in interchange

	if (existingStationName)
	{
		for (int i = 0; i < MAX_SIZE; i++)
		{
			Node* current = lines[i];

			while (current != NULL)
			{
				if (current->name == name)
				{
					for (int i = 0; i < INTERCHANGE_SIZE; i++)
					{
						if (current->interchanges[i] == "")
						{
							current->interchanges[i] = code;
							break;
						}
					}
					for (int i = 0; i < INTERCHANGE_SIZE; i++)
					{
						if (newNode->interchanges[i] == "")
						{
							newNode->interchanges[i] = current->code;
							break;
						}
					}
				}
				current = current->next;
			}
		}
	}

	Node* current = lines[index];	// moves current to correct line
	while (current->code != reference)
	{
		current = current->next;
	}

	// Scenarios
	// 1. if 'b' with first node 
	// 2. if 'a' after last node
	// 3. if everything else in between

	if (current->previous == NULL)		// current is at the first station of the line
	{
		// add new station before the first station
		if (beforeafter == 'b')
		{
			newNode->distance = distance;
			newNode->next = current;
			current->previous = newNode;
			lines[index] = newNode;
		}
		// add new station after the first station
		else
		{
			newNode->previous = current;
			newNode->next = current->next;
			if (newNode->next != NULL)
			{
				newNode->distance = distance;
				(current->next)->previous = newNode;
			}
			else
			{
				current->distance = distance;
			}
			current->next = newNode;
		}
	}
	else if (current->next == NULL)		// current is at the last station of the line
	{
		// taking the last node (station) into account 
		if (beforeafter == 'a')
		{
			current->distance = distance;
			newNode->distance = 0;
			current->next = newNode;
			newNode->previous = current;
		}
		else
		{
			newNode->distance = distance;
			newNode->next = current;
			newNode->previous = current->previous;
			(current->previous)->next = newNode;
			current->previous = newNode;
		}
	}
	else // current is in the middle of the line
	{
		if (beforeafter == 'b')
		{
			newNode->distance = distance;
			newNode->next = current;
			newNode->previous = current->previous;
			(current->previous)->next = newNode;
			current->previous = newNode;
		}
		else
		{
			newNode->distance = distance;
			newNode->previous = current;
			newNode->next = current->next;
			(current->next)->previous = newNode;
			current->next = newNode;
		}
	}
	return true;
}


// Option 4 : Find and display a route and its price, given the source and destination (returns distance of route)
int Map::getRoute(string Scode, string current_Dcode, string final_Dcode) //Skip a station if it doesnt exist.
{
	// test case 1. On the same Line.
	int Scodeno = stoi(Scode.substr(2, 3));		// gets number out of a station code e.g 19 from EW19
	int Dcodeno = stoi(current_Dcode.substr(2, 3));

	if (stationValidityCode(Scode))		// checks if station exists
	{
		Node* current = returnNode(Scode);

		if (Scode.substr(0, 2) == current_Dcode.substr(0, 2))	// if both stations are on the same line
		{
			if (Scode == current_Dcode)		// if both stations are the same (reached interchange/destination)
			{
				if (isInterchange(current_Dcode))
				{
					if (current_Dcode == final_Dcode)
					{
						cout << "Reached " << current_Dcode << " (Destination)" << endl;
					}
					else
					{
						cout << "Moved to " << current_Dcode << endl;
						cout << endl << "At " << current_Dcode << ", switch lines to " << returnNode(current_Dcode)->interchanges[0] << endl << endl;
					}
				}
				else
				{
					cout << "Reached " << current_Dcode << " (Destination)" << endl;
				}
				return 0;
			}
			else
			{
				if (Scodeno < Dcodeno)	// e.g. EW1, EW2, EW3, EW4 (ascending)
				{
					cout << "Moved to " << Scode << endl;
					string newScode = Scode.substr(0, 2) + to_string(Scodeno + 1);	// why not use current->next?
					return current->distance + getRoute(newScode, current_Dcode, final_Dcode);		// Deepest
				}
				else	// e.g. EW4, EW3, EW2, EW1 (descending)
				{
					cout << "Moved to " << Scode << endl;
					string newScode = Scode.substr(0, 2) + to_string(Scodeno - 1);
					return current->previous->distance + getRoute(newScode, current_Dcode, final_Dcode);
				}
			}
		}
		if (Scode.substr(0, 2) != current_Dcode.substr(0.2))	// if both station codes are of different lines (e.g. EW22 -> NS3)
		{
			// traverse to an interchange on the source's line that matches the destination's line and is closest to the destination.
			return getRoute(current->code, changeLine(current->code, current_Dcode)->code, final_Dcode) + getRoute(changeLine(current->code, current_Dcode)->interchanges[0], current_Dcode, final_Dcode);
			// ^ this line basically returns the distance between source and the interchange
		}
	}
	else	// skip station with code that does not exist (e.g. NS6)
	{
		if (Scodeno < Dcodeno)
		{
			string newScode = Scode.substr(0, 2) + to_string(Scodeno + 1);
			return 0 + getRoute(newScode, current_Dcode, final_Dcode);
		}
		else
		{
			string newScode = Scode.substr(0, 2) + to_string(Scodeno - 1);
			return 0 + getRoute(newScode, current_Dcode, final_Dcode);
		}
	}
}


// Option 5 : Add a new line (with a first station)
void Map::addfirstStationOfLine(string code, string name)
{
	Node* newNode = new Node;
	newNode->code = code;
	newNode->name = name;
	newNode->previous = NULL;
	newNode->next = NULL;
	newNode->distance = 0;

	bool existingStationName = stationValidityName(name);	// true = add existing station codes in interchange

	if (existingStationName)
	{
		for (int i = 0; i < MAX_SIZE; i++)
		{
			Node* current = lines[i];

			while (current != NULL)
			{
				if (current->name == name)
				{
					for (int i = 0; i < INTERCHANGE_SIZE; i++)
					{
						if (current->interchanges[i] == "")
						{
							current->interchanges[i] = code;
							break;
						}
					}
					for (int i = 0; i < INTERCHANGE_SIZE; i++)
					{
						if (newNode->interchanges[i] == "")
						{
							newNode->interchanges[i] = current->code;
							break;
						}
					}
				}
				current = current->next;
			}
		}
	}
	lines[getLineIndex(code)] = newNode;
	size++;
}


// to determine if both stations given are the same station
bool Map::sameStation(string Scode, string Dcode)
{
	if (Scode == Dcode)
	{
		return true;
	}
	else {
		for (int i = 0; i < INTERCHANGE_SIZE; i++)
		{
			if (returnNode(Scode)->interchanges[i] == Dcode)
			{
				return true;
			}
		}
		return false;
	}
}


// to determine if a station is an interchange
bool Map::isInterchange(string code)
{
	Node* current = returnNode(code);
	if (current->interchanges[0] == "")
	{
		return false;
	}
	else return true;
}


// to retrieve all interchanges of a station and store them into an array, given the station code
List Map::interchangeArray(string code)
{
	Node* current = lines[getLineIndex(code)];
	List l;
	while (current != NULL)
	{
		if (current->interchanges[0] != "")
		{
			l.add(current->code);
		}
		current = current->next;
	}
	return l;
}


// to return a station interchange's alternate station
string Map::returnInterchangeStation(string code)
{
	Node* current = returnNode(code);
	return current->interchanges[0];
}