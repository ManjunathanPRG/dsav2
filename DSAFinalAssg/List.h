#include <iostream>
using namespace std;

const int MAX_SIZE_LIST = 10;		// assuming 1 line only stores 10 interchanges at most
typedef string ItemType;

class List
{
private:
	ItemType items[MAX_SIZE_LIST];
	int size;

public:

	// constructor
	List();

	// to add an item at the end of the list (append)
	bool add(ItemType item);

	// to retrieve an item in the array, given its index
	ItemType getArrayItem(int index);
};