//Map.h
#include "List.h"
#include <string>
#include <iostream>
using namespace std;

const int MAX_SIZE = 30;
const int INTERCHANGE_SIZE = 5;

class Map
{
private:
	struct Node
	{
		string name;	// name of the station
		string code;	// code of the station
		string interchanges[INTERCHANGE_SIZE];	// stores station codes of different lines at the same station, up to 5 interchanges
		Node* next;		// pointer to next station
		Node* previous;	// pointer to previous station
		int distance;	// distance of station to the next station
	};

	string codes[MAX_SIZE];
	Node* lines[MAX_SIZE];	// an array that stores 30 nodes, the nodes are the first node of each line.
	int size;

public:
	// constructor
	Map();

	// to retrieve the node of a station, given its station code
	Node* returnNode(string code)
	{
		Node* current = lines[getLineIndex(code)];
		while (current->code != code)
		{
			current = current->next;
		}
		return current;
	}

	// to change from one line to another, returning the node of the station to change line
	Node* changeLine(string Scode, string Dcode)
	{
		// traverse to the node of the current station
		Node* current = returnNode(Scode);
		Node* movePrevious = current;
		Node* moveNext = current;
		string destination_code = Dcode.substr(0, 1);	// gets the line out of the station code (e.g. EW from EW19)
		Node* stationNext = NULL;				// stores node of next station that has interchange to destination line
		Node* stationPrevious = NULL;			// stores node of previous station that has interchange to destination line
		string stationNext_DlineCode = "";		// stores the station code of the destination station line (e.g. EW22 -> NS3, stores NS1)
		string stationPrevious_DlineCode = "";	// stores the station code of the destination station line (e.g. EW22 -> NS3, stores NS26)

		// to traverse from the source's station to each station in ascending order, finding closest station with interchange to destination code
		while (moveNext != NULL)
		{
			for (int i = 0; i < INTERCHANGE_SIZE; i++)
			{
				if ((moveNext->interchanges[i]).substr(0, 1) == destination_code)
				{
					stationNext = moveNext;
					stationNext_DlineCode = moveNext->interchanges[i];
				}
			}
			if (stationNext != NULL)
			{
				break;
			}
			else
			{
				moveNext = moveNext->next;
			}
		}

		// to traverse from the source's station to each station in descending order, finding closest station with interchange to destination code
		while (movePrevious != NULL)
		{
			for (int i = 0; i < INTERCHANGE_SIZE; i++)
			{
				if ((movePrevious->interchanges[i]).substr(0, 1) == destination_code)
				{
					stationPrevious = movePrevious;
					stationPrevious_DlineCode = movePrevious->interchanges[i];
				}
			}
			if (stationPrevious != NULL)
			{
				break;
			}
			else
			{
				movePrevious = movePrevious->previous;
			}
		}

		// checks if one of the two variables found a station with interchange to destination line
		if (stationNext != NULL || stationPrevious != NULL)
		{
			// checks if both variables found a station with interchange to destination line
			// if true, compare and return station closest to destination station
			if (stationNext != NULL && stationPrevious != NULL)
			{
				int stationNext_codeNo = stoi(stationNext_DlineCode.substr(stationNext_DlineCode.find(destination_code) + destination_code.length()));
				int stationPrevious_codeNo = stoi(stationPrevious_DlineCode.substr(stationPrevious_DlineCode.find(destination_code) + destination_code.length()));
				int destination_codeNo = stoi(Dcode.substr(Dcode.find(destination_code) + destination_code.length()));

				int next_difference, previous_difference;
				next_difference = getDifference(stationNext_codeNo, destination_codeNo);
				previous_difference = getDifference(stationPrevious_codeNo, destination_codeNo);

				if (next_difference < previous_difference)
				{
					return stationNext;
				}
				else
				{
					return stationPrevious;
				}
			}
			else if (stationNext != NULL)
			{
				return stationNext;
			}
			else
			{
				return stationPrevious;
			}
		}
	}

	// to return a positive difference between two numbers (to compare station codes to get difference in number of stops)
	int getDifference(int a, int b)
	{
		if (a < b)
		{
			return (b - a);
		}
		else
		{
			return (a - b);
		}
	}

	// to retrieve all station data from Stations.csv 
	bool addStationData(string code, string name);

	// to retrieve all station distances from Routes.csv
	void loadDistance(string code, string distance);

	// to retrieve and store all stations with 2 interchanges from Interchanges.csv
	void loadInterchange(string s1, string s2);

	// to retrieve and store all stations with 3 interchanges from Interchanges.csv
	void loadInterchange(string s1, string s2, string s3);

	// to retrieve the index of a line (EW, CC, NS) in the lines array, given the station code
	int getLineIndex(string code);

	// to determine if a station exists, given the station code
	bool stationValidityCode(string code);

	// to determine if a station exists throughout all lines, given the station name
	bool stationValidityName(string name);

	// to determine if a line exists
	bool lineValidity(string line);

	// to determine if a station exists in a line, given the station name and code
	bool stationExistInLine(string name, string code);

	// to display all stations of a line
	void displayStationsOfLine(string code);

	// to retrieve station information, given the station name
	void getStationInfo(string name);

	// to add a station to a line
	bool addStation(string code, string name, char beforeafter, string referencecode, int distance);

	// to find the route of a journey from one station to another, given a source and destination station
	int getRoute(string Scode, string current_Dcode, string final_Dcode);

	// to add the first station of a new line
	void addfirstStationOfLine(string code, string name);

	// to determine if both stations given are the same station
	bool sameStation(string Scode, string Dcode);

	// to determine if a station is an interchange
	bool isInterchange(string code);

	// to retrieve all interchanges of a station and store them into an array, given the station code
	List interchangeArray(string code);

	// to return a station interchange's alternate station
	string returnInterchangeStation(string code);
};