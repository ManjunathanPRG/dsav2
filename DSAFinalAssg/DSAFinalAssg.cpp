#include <iostream>
#include "Map.h"
#include "fstream"
#include <string>
#include <algorithm>
using namespace std;

void MainMenu();


// |				    DATA INITIALIZATION						|

void LoadInterchange();
void LoadDistance();
void InitData();



// |					 OPTIONS FUNCTIONS						|

void DisplayLine();						// 1
void GetStationInfo();						// 2
void AddStation();							// 3
void FindRoute();						// 4 (route/distance)
void AddNewLine();						// 5
void FindMorePath();					// 6 (finds many routes)
double CalculateFare(double distance);	// 4/6 (fare)


Map map;


int main()
{
	InitData();
	string option = "1";

	while (option != "0")
	{
		MainMenu();
		cin >> option;
		cout << endl;
		cin.ignore();

		if (option == "1")
		{
			DisplayLine();
		}
		else if (option == "2")
		{
			GetStationInfo();
		}
		else if (option == "3")
		{
			AddStation();
		}
		else if (option == "4")
		{
			FindRoute();
		}
		else if (option == "5")
		{
			AddNewLine();
		}
		else if (option == "6")
		{
			FindMorePath();
		}
		else if (option == "0")
		{
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			cout << "Shutting down the program..." << endl;
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		}
		else
		{
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			cout << "Invalid option entered!" << endl;
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		}
	}
}


void MainMenu()
{
	cout << endl;
	cout << "********************************************************" << endl;
	cout << "\t\t\tDSA MRT System" << endl;
	cout << "---------------------------------------------------------" << endl;
	cout << "[1] Display all stations in an existing MRT line" << endl;
	cout << "[2] Display station information with given station name" << endl;
	cout << "[3] Add and save a new station in an existing MRT line" << endl;
	cout << "[4] Find and display a route and its price" << endl;
	cout << "[5] Add a new line into the system" << endl;
	cout << "[6] Display 3 possible routes with their price and distance respectively" << endl;
	cout << "[0] Exit System" << endl;
	cout << "*********************************************************" << endl;
	cout << "Enter option : ";
}



//|										DATA INIITIALIZATION								  	  |

void LoadInterchange()
{
	ifstream file;
	file.open("Interchanges.csv");
	while (!file.eof())
	{
		string s1;
		string s2;
		string s3;
		getline(file, s1, ',');
		getline(file, s2, ',');
		getline(file, s3);	// if an interchange only got 2 stations, s3 should be ""
		if (s3 == "")
		{
			map.loadInterchange(s1, s2);
			map.loadInterchange(s2, s1);
		}
		else
		{
			map.loadInterchange(s1, s2, s3);
			map.loadInterchange(s2, s1, s3);
		}
	}
	file.close();
}


void LoadDistance()
{
	ifstream file;
	file.open("Routes.csv");
	while (!file.eof())
	{
		string allcodes;
		string alldistances;
		getline(file, allcodes);
		int comma_count = count(allcodes.begin(), allcodes.end(), ',') + 1;
		getline(file, alldistances);
		int i = 0;

		while (i < comma_count)
		{
			int codeindex = allcodes.find(',');
			int distanceindex = alldistances.find(',');
			string code;
			string distance;

			code = allcodes.substr(0, codeindex);
			distance = alldistances.substr(0, distanceindex);
			map.loadDistance(code, distance);
			allcodes = allcodes.erase(0, codeindex + 1);
			alldistances = alldistances.erase(0, distanceindex + 1);
			i++;
		}
	}
	file.close();
}


void InitData()
{
	ifstream file;
	file.open("Stations.csv");
	string code;
	string name;

	while (!file.eof())
	{
		getline(file, code, ',');
		getline(file, name);
		map.addStationData(code, name);
	}
	file.close();

	LoadDistance();
	LoadInterchange();
}
//END



//|										OPTION FUNCTIONS										  |

// Option 1 : Display all stations in an existing line
void DisplayLine()
{
	cout << "Enter an existing line code to display all stations : ";
	string code;
	cin >> code;
	transform(code.begin(), code.end(), code.begin(), ::toupper);
	map.displayStationsOfLine(code);
	
}


// Option 2 : Display station information
void GetStationInfo()
{
	cout << "Enter the name of the station you would like the information for : ";
	string name;
	getline(cin, name);

	if (!map.stationValidityName(name))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "ERROR: No station found!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else
	{
		cout << endl << "----------------------------------" << endl;
		cout << "Station name : " << name << endl;
		cout << "----------------------------------" << endl;
		map.getStationInfo(name);
	}
}


// Option 3 : Add and save a new station in an existing line
// Validation scenarios
// ================================
// check if new station code is already existing (break if existing)						== done
// check if new station name is already existing in same line (break if existing)			== done
// check if reference station name is already existing (break if not existing)				== done
// check if reference station code is another line											== done
// check if station name is also existing station (this new station will be interchanges)	== done
	// e.g. add new station NS6 called Hillview, which exists in DT3
	// add For NS6 Hillview, add DT3 in interchange
	// add For DT3 Hillview, add NS6 in interchange
void AddStation()
{
	string code;
	char beforeafter;
	cout << "Enter the new stations code in the following format -> (Line Code + Station Number) : ";
	cin >> code;
	if (map.stationValidityCode(code))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << code << " is an existing station!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else
	{
		cin.ignore();
		string name, reference, distance;
		cout << "Enter new station name : ";
		getline(cin, name);
		if (map.stationExistInLine(name, code.substr(0, 2)))
		{
			cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			cout << name << " is an existing station in the system!" << endl;
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		}
		else
		{
			cout << "Enter the station code to reference (Station before or after the new station) : ";
			cin >> reference;
			if (!map.stationValidityCode(reference))
			{
				cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
				cout << reference << " is not an existing station in the line!" << endl;
				cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			}
			else if (reference.substr(0, 2) != code.substr(0, 2))
			{
				cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
				cout << reference << " is a station from another line!" << endl;
				cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			}
			else
			{
				cout << "Enter new station before or after the referenced station [a/b] : ";
				cin >> beforeafter;
				cout << "Enter the distance of the new station to the next station in metres : ";
				cin >> distance;
				map.addStation(code, name, beforeafter, reference, stoi(distance));
				cout << "Station created : " << endl << "Station code : " << code << endl << "Station name : " << name << endl;
			}
		}
	}
}


// Option 4 : Find and display a route and its price, given the source and destination
// invalid station entered (NS6, NS9)								== done
// same source and destination station code (NS1, NS1)				== done
// same source and destination stations, different code (NS1, EW24) == done
// stations on same line (NS1, NS9)									== done
// stations on another line (NS3, EW20) - 1 interchange only		== done
// stations on another line (NS3, EW20) - 2 interchange (CE, CG)		
void FindRoute()
{
	string source, destination;
	int distance = 0;
	cout << "Enter current station code : ";
	cin >> source;
	cout << "Enter destination code : ";
	cin >> destination;
	cout << endl;
	if (!map.stationValidityCode(source) || !map.stationValidityCode(destination))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "Invalid station code(s) entered!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else if (map.sameStation(source, destination))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "Same station entered!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else
	{
		//checks for CG bc programme reads 
		if (destination.substr(0, 2) == "CG" && (source.substr(0, 2) == "CG"))
		{
		distance = map.getRoute(source, destination, destination);
		}
		//checks if dest and source part of CG, gets route
		else if (source.substr(0, 2) == "CG")
		{
			distance = map.getRoute(source, "CG0", destination) + map.getRoute("EW4", destination, destination);
		}
		// check if source is part of CG line, if it is, gets route from source to CG0
		// EW4 = CG0, gets route from EW4 --> Destination; appends route to CG route
		else if (destination.substr(0, 2) == "CG")
		{
			distance = map.getRoute(source, "EW4", destination) + map.getRoute("CG0", destination, destination);
		}
		// check if destination is part of CG line, if it is, gets route from source to EW4
		// EW4 = CG0, gets route from CG0 --> Destination; appends route to EW route
		else
		{
			distance = map.getRoute(source, destination, destination);
		}
		// if source and dest not part of CG, gets route
		cout << endl << "--------------------------" << endl;
		cout << "Route Information" << endl;
		cout << "--------------------------" << endl;
		cout << "Distance : " << distance << " metres" << endl;
		cout << "Cost : $" << CalculateFare(distance) / 100 << "0" << endl;
		cout << "--------------------------" << endl;
	}
}


// Option 5 : Add a new line
void AddNewLine()
{
	string line, firstStationCode, firstStationName;
	cout << "Enter the new line code : ";
	cin >> line;
	if (map.lineValidity(line))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "Line " << line << " already exists!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else
	{
		cout << "Enter the station code for the first station : ";
		cin >> firstStationCode;
		if (firstStationCode.substr(0, 2) != line)
		{
			cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
			cout << "Station code entered does not match line above!" << endl;
			cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		}
		else
		{
			cin.ignore();
			cout << "Enter the station name for the first station : ";
			getline(cin, firstStationName);
			map.addfirstStationOfLine(firstStationCode, firstStationName);
			cout << endl << "----------------------------" << endl;
			cout << "Line " << line << " has been created!" << endl;
			cout << "----------------------------" << endl;
		}
	}
}


// Option 6 : Display up to 3 possible routes with their price and distance
void FindMorePath()
{
	//int distance = map.getRoute(source,destination)
	//int distance2 = map.getRoute(source,interchange)+(interchange,destination)
	string source, destination;
	cout << "Enter current station code : ";
	cin >> source;
	cout << "Enter destination code : ";
	cin >> destination;
	if (!map.stationValidityCode(source) || !map.stationValidityCode(destination))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "Invalid station code(s) entered!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else if (map.sameStation(source, destination))
	{
		cout << endl << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
		cout << "Same station entered!" << endl;
		cout << ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>" << endl;
	}
	else
	{
		cout << endl;
		List interchanges = map.interchangeArray(source);
		int i = 0;

		if (source.substr(0, 2) == destination.substr(0, 2))
		{
			cout << "\n================  Alternate Route  ================\n\n";
			int distance = map.getRoute(source, destination, destination);

			cout << endl << "--------------------------" << endl;
			cout << "Route Information" << endl;
			cout << "--------------------------" << endl;
			cout << "Distance : " << distance << " metres" << endl;
			cout << "Cost : $" << CalculateFare(distance) / 100 << "0" << endl;
			cout << "--------------------------" << endl << endl;
		}

		while (i < MAX_SIZE && interchanges.getArrayItem(i) != "" && i<4)	// Max size interchange array can store, shows only 3 routes
		{
			if (destination.substr(0, 2) == "CG")	// if special case CG
			{
				if (map.returnInterchangeStation(interchanges.getArrayItem(i)).substr(0, 2) == "CE")
				{
					i++;
					cout << endl << "Another route has been calculated but its route is unapplicable" << endl;
					continue;
				}
				else
				{
					cout << "\n================  Alternate Route  ================\n\n";
					// Traverse out via CG0 - EW4 only.
					int distance1 = map.getRoute(source, interchanges.getArrayItem(i), destination);		// Source
					int distance2 = map.getRoute(map.returnInterchangeStation(interchanges.getArrayItem(i)), "EW4", destination);
					int distance3 = map.getRoute(map.returnInterchangeStation("CG0"), destination, destination);
					int total_distance = distance1 + distance2 + distance3;

					cout << endl << "--------------------------" << endl;
					cout << "Route Information" << endl;
					cout << "--------------------------" << endl;
					cout << "Distance : " << total_distance << " metres" << endl;
					cout << "Cost : $" << CalculateFare(total_distance) / 100 << "0" << endl;
					cout << "--------------------------" << endl << endl;
				}

			}
			else if (destination.substr(0, 2) == "CE")	// if special case CE
			{
				// Traverse out via CE2 - NS27 only.
				cout << "\n================  Alternate Route  ================\n\n";
				int distance1 = map.getRoute(source, interchanges.getArrayItem(i), destination);		// Source
				int distance2 = map.getRoute(map.returnInterchangeStation(interchanges.getArrayItem(i)), "NS27", destination);
				int distance3 = map.getRoute(map.returnInterchangeStation("CE2"), destination, destination);
				int total_distance = distance1 + distance2 + distance3;

				cout << endl << "--------------------------" << endl;
				cout << "Route Information" << endl;
				cout << "--------------------------" << endl;
				cout << "Distance : " << total_distance << " metres" << endl;
				cout << "Cost : $" << CalculateFare(total_distance) / 100 << "0" << endl;
				cout << "--------------------------" << endl << endl;
			}
			else
			{
				if (map.returnInterchangeStation(interchanges.getArrayItem(i)).substr(0, 2) == "CG" || map.returnInterchangeStation(interchanges.getArrayItem(i)).substr(0, 2) == "CE")
				{
					i++;
					continue;
				}
				else
				{
					cout << "\n================  Alternate Route  ================\n\n";
					int distance1 = map.getRoute(source, interchanges.getArrayItem(i), destination);//EW->EW
					int distance2 = map.getRoute(map.returnInterchangeStation(interchanges.getArrayItem(i)), destination, destination);
					int total_distance = distance1 + distance2;
					cout << endl << "--------------------------" << endl;
					cout << "Route Information" << endl;
					cout << "--------------------------" << endl;
					cout << "Distance : " << total_distance << " metres" << endl;
					cout << "Cost : $" << CalculateFare(total_distance) / 100 << "0" << endl;
					cout << "--------------------------" << endl << endl;
				}
			
			}
			i++;

		}
		
	}
}


//map.getRoute(source, interchanges[i]);
//map.getRoute(map.returnInterchangeStation(interchanges[i]), destination);


// Option 4 & 6 : returns the fare of a route, given its distance
double CalculateFare(double distance)
{
	ifstream file;
	file.open("Fares.csv");
	string filedistance, filecost;
	double distanceinkm = distance / 1000;
	double convertdistance;
	double cost = 0;

	while (!file.eof())
	{
		double current = 0;
		getline(file, filedistance, ',');
		getline(file, filecost);
		convertdistance = stod(filedistance);

		if (convertdistance <= distanceinkm)
		{
			cost = stod(filecost);
		}
	}
	return cost;
	file.close();
}
//END